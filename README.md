# Docker & Docker Swarm

## Run In Swarm

```bash
wget -O - https://gitlab.com/rekamy/docker-training/-/raw/main/testapp/docker-compose.yml | tee stack.yml

docker stack deploy -c stack.yml app
```

## Using Swarmpit

1. Install swarmpit

    ```bash
    docker run -it --rm \
    --name swarmpit-installer \
    --volume /var/run/docker.sock:/var/run/docker.sock \
    swarmpit/install:1.9
    ```

2. Copy content from

    <https://gitlab.com/rekamy/docker-training/-/raw/main/testapp/docker-compose.yml>

3. Paste into swarmpit stack

## Available endpoint

1. [Home Page](http://139.162.25.212/)
2. [Swarmpit](http://139.162.25.212:8080/)
3. [Horizon](http://139.162.25.212/horizon)
4. [Pulse](http://139.162.25.212/pulse) (Require Login)
5. [Go Access](http://139.162.25.212/monitor)
6. [Visualizer](http://139.162.25.212:8000/)

## Credential

1. Laravel

    ```txt
    Email: admin@mail.com
    Password: password
    ```

2. Swarmpit

    ```txt
    Email: admin
    Password: standart rekamy
    ```
