<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::view('/','welcome');

Route::get('/users', function () {
    $users = \App\Models\User::inRandomOrder()->take(100)->get();
    $noOfUser = \App\Models\User::count();
    Auth::login($users->first());
    return view('users', compact('users', 'noOfUser'));
});

Route::get('/users/all', function () {
    $users = \App\Models\User::inRandomOrder()->get();
    $noOfUser = \App\Models\User::count();
    Auth::login($users->first());
    return view('users', compact('users', 'noOfUser'));
});

Route::get('/upload', function () {
    $uuid = \Str::uuid();
    $path = Storage::put("{$uuid}.json", 'Contents');
    // Storage::disk
    return "<a href=\"{$uuid}.json\">";
    // return view('dashboard');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
