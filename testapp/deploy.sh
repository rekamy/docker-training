repo=local/php

## LOCAL TEST COMPOSE
docker compose down
docker build --build-arg APP_ENV=production -t $repo .
docker compose up -d
echo ''
echo '   Host URL:   http://localhost/  '

## LOCAL TEST STACK
# docker stack rm app
# docker build --build-arg APP_ENV=production -t $repo .
# docker stack deploy -c docker-compose.yml app
# echo ''
# echo '   Host URL:   http://host.docker.internal/  '
