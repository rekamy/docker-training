if [ -z "$1" ]; then
    # start php-fpm service daemon
    echo 'Please input your caprover server password like below'
    echo 'Ex:'
    echo '   sh ./deploy.sh password'
    exit
fi

curdate=$(date +"%Y%m%d%H%M")
repo=registry.gitlab.com/rekamy/my-project-name:${curdate}

docker login registry.gitlab.com \
&& docker build --build-arg APP_ENV=staging -t ${repo} . \
&& docker push ${repo} \
&& docker run -it --name caprover caprover/cli-caprover:v2.1.1 caprover deploy -u captain.rekamy.com -p $1 -a caprover-app-name -i ${repo}
&& docker rm caprover
