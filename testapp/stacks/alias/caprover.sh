#!/bin/bash

alias dk='docker $*'
alias dkprune='docker container prune --force && docker image prune --all && docker system prune -f'

function dkr () {
        if [ -z "$2"]
        then
        docker exec -it $1 bash;
        else
        docker exec -it $1 $2;
        fi
};

alias ds='docker service $*'
alias tdsr='echo $(docker ps -q -f name=srv-captain--$1)'

function dsr () {
        if [ -z "$2"]
        then
        docker exec -it $(docker ps -q -f name=srv-captain--$1) bash;
        else
        docker exec -it $(docker ps -q -f name=srv-captain--$1) $2;
        fi
}

alias dsl='docker service ls'

function dsi () {
        docker service inspect srv-captain--$1;
}

cp ./stacks/caprover/.env.production ./.env.production
cp ./stacks/caprover/images/fpm/docker-compose.yml ./docker-compose.yml
docker compose up -d
