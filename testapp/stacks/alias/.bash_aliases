#!/bin/bash

alias dk='docker $*'
alias dkprune='docker system prune -f'

function dkr () {
        docker exec -it $1 $2;
};

alias ds='docker service $*'
alias tdsr='echo "srv-captain--$1"'

function dsr () {
        docker exec -it $(docker ps -q -f name=srv-captain--$1) $2;
}

alias dsl='docker service ls'

function dsi () {
        docker service inspect srv-captain--$1;
}
