rm -rf ./stacks/.git
cp ./stacks/setup/.env.staging ./
cp ./stacks/setup/.gitlab-ci.yml ./
cp ./stacks/images/fpm/Dockerfile ./
cp ./stacks/setup/docker-stack.yml ./
cp ./stacks/setup/caprover-deploy.sh ./deploy.sh

docker build -t local/php .
docker swarm init
docker stack rm app
docker stack deploy --compose-file docker-stack.yml app

echo ''
echo '   Host URL:   http://localhost/  '
