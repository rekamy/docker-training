
# **Reka Stacks**

## **Quick Start**

1. Clone Rekamy Stack inside your project

    ```bash
    git clone --depth=1 https://rekamy:pYAv5XmgB2D_87qAcLiZ@gitlab.com/rekamy/packages/stacks.git
    sh ./stacks/boot.sh
    ```

    > Specific for Laravel Apps, you may install s3 to achieve stateless container and horizon to manage daemon queue and scheduler

    ```bash
    composer require laravel/horizon league/flysystem-aws-s3-v3 --ignore-platform-req=ext-pcntl --ignore-platform-req=ext-posix
    php artisan horizon:install
    ```

2. Create an app in caprover server with your preferred name

3. Go to your repository Setting > CI/CD and expand Variable section

    > your may use this link to directly go to your CICD setting <https://gitlab.com/{org}/{repo}/-/settings/ci_cd>

4. Add variable with:
    - key: `CAPROVER_APP`
    - value: `<Your App Name In Caprover Server>`

5. Push your local source code and wait for the magic at your public hosted endpoint : <http://{app_name}.rekamy.com> **OR** deploy manually by :

    ```bash
    sh deploy.sh <password for caprover server>
    ```

## **How to use**

Create a new laravel project if you dont have any. and initialize as git project

> **_NOTE:_**  Skip this step if you start from existing project repository

```bash
laravel new <app_name>
cd <app_name>
git init
```

Go to your project root directory and clone this package.

```bash
git clone https://rekamy:pYAv5XmgB2D_87qAcLiZ@gitlab.com/rekamy/packages/stacks.git
sh ./stacks/boot.sh
```

---

## **Using Captain Definition (For Caprover Base Server)**

Copy `captain-definition` file to your project root directory and edit the file accordingly

```bash
cp ./stacks/setup/captain-definition ./
```

> **_NOTE:_**  This section use fpm and nginx webserver proxy setup as example

Build your own image (Dockerfile) at project root directory or you may choose any image provided in the `./stacks/images` directory as a start and edit the file accordingly specifically at **PROJECT SETUP SECTION** where you may need to provide different `environment variable` file for your production server.

Replace `fpm` with any webserver image directory available in `stacks`

```bash
cp ./stacks/images/fpm/Dockerfile ./
```

Copy `.gitlab-ci.yml` file to your project root directory and edit the file accordingly

```bash
cp ./stacks/setup/.gitlab-ci.yml ./
```

> **Remember create & edit your `.env.staging` file in your root folder accordingly before commit or test your local compose setup**

To test your setup in local before commit to production, you may copy `docker-compose.yml` from stacks that represent exactly as caprover setup.

```bash
cp ./stacks/setup/.env.production ./.env.production
cp ./stacks/images/fpm/docker-compose.yml ./docker-compose.yml
docker compose up -d
```

Then you you can browse your application at <http://localhost>

### **Production environment variable strategy**

The easiest way to setup production `environment variable` are by providing `.env.production` file that already contain your production settings. However, this strategy are not the best since the production `environment variable` might contain credential that should not be shared.

Unfortunately, this stacks does not provide an opinionated way for production `environment variable`. You may design it youself and configure your image accordingly.

Current images provide by `./stacks/images` images are depend on `.env.example` as your production `environment variable` reference. Thus, please do edit the image accordingly.

### **Caprover setup**

#### **Application Service Container**

#### **Method 1:** Deployment Using Gitlab CI/CD

To use gitlab CI/CD, by default

1. Create an app without `Persistent Data`.
2. Go to your repository at version control portal.
3. On the sidebar of your repository, go to `Settings > CI/CD > Variables` and add new variable
    - key: `CAPROVER_APP`
    - value: `<Your App Name In Caprover Server>`

> It is required for branch that apply CI/CD to be protected to reduce build cycle repetition
>
> On the sidebar of your repository, go to `Settings > Repositories > Protected branches` and add stable branch as protected

#### **Method 2:** Deployment Using Gitlab Webhook

1. Create an app named as `<app_name>` without `Persistent Data`.
2. Click on your created app and go to `Deployment` tab.
3. Scroll down to `Method 3: Deploy from Github/Bitbucket/Gitlab` section and add server SSH key. You may find this from `testapp` deployed in the same server.
4. Add repository name as per instructed in the placeholder (without `http / https` protocol, without `git@` prefix, and without `.git` suffix).  
5. Specify branch and click `Save & Update`.
6. Copy the webhook url right after you `Save & Update`.
7. Go to your repository at version control portal.
8. On the sidebar of your repository, go to Settings > Webhooks and create new webhook with `push` or `release` event by paste webhook URL copied earlier. This choice between `push` or `release` depends on your server need. If its for production, it is recommended to deploy only if release version are created.
9. On the sidebar of your repository, go to `Settings > Repository > Deploy Key` and expand the tab. Add new SSH key of your server, or enable it from private accessible deploy key. For example: Rekamy Caprover-01.

---

## **Using Compose (For Docker Desktop or Docker Base Server)**

Copy the `docker-compose.yml` file to your project root directory and edit the file accordingly

```bash
cp ./stacks/docker-compose.yml ./
```

Ensure your environment variable are pointing to the right container name

```env
APP_URL=https://app.localhost
...
DB_HOST=mariadb
DB_PASSWORD=password
...
REDIS_HOST=redis
...
MAIL_HOST=mailpit
```

Run your docker instance

```bash
docker compose up -d
```

Give it a second to initialize all the service and then you can check your services up and running.

1. Application `https://app.localhost`
2. Meilisearch `https://meilisearch.localhost`
3. Minio `https://minio.localhost`
4. Mailpit `https://mail.localhost`

For new Laravel project, you might need to run certain command to bootstrap your Laravel application.

```bash
docker compose exec -it php bash
php artisan key:generate
php artisan migrate
```

If you are using swoole as server, you might need to install laravel octane and horizon from composer and chokidar from npm.

```bash
docker compose exec -it php bash
composer require laravel/octane laravel/horizon 
npm i chokidar --save-dev
php artisan octane:install
php artisan horizon:install
```

If you serving octane behind SSL, add this environment variable in `.env` file

```environment
OCTANE_HTTPS=true
```

More info on laravel octane, `https://laravel.com/docs/9.x/octane#installation`

More info on laravel horizon, `https://laravel.com/docs/9.x/horizon#installation`

If you are require minio or s3, you might need to install Flysystem S3 package.

```bash
docker compose exec -it php bash
composer require league/flysystem-aws-s3-v3 "^3.0"
```

More info on laravel S3, `https://laravel.com/docs/9.x/filesystem#s3-driver-configuration`, `https://laravel.com/docs/9.x/filesystem#amazon-s3-compatible-filesystems`

If you are require meilisearch, you might need to install laravel scout and meilisearch.

```bash
docker compose exec -it php bash
composer require laravel/scout meilisearch/meilisearch-php http-interop/http-factory-guzzle
```

More info on laravel scout, `https://laravel.com/docs/9.x/scout#installation`

You may use this command for both

```bash
docker compose exec -it php bash
composer require laravel/octane laravel/horizon laravel/scout meilisearch/meilisearch-php http-interop/http-factory-guzzle && npm i chokidar --save-dev
```

You might need to restart docker php instance after running this command

```bash
docker compose stop php
docker compose start php
```

---

## **Post installation**

In order to allow easy implementation of this package, all configuration are not being skip from version control of this package by default. Thus, any changes you made may change the original package library.

To avoid tampering with the original packages, please remove this stacks `.git` folder and remove git module reference to this package. With this, you may no longer connected to this package repository.

```bash
rm -rf ./stacks/.git
```

Only remove git modules using this command if you dont have any other submodule. If you do, please remove it manually.

```bash
rm -rf ./.gitmodules
```

Remove indexing on stacks folder if previously you have index it in git

```bash
git rm --cached stacks
git add . 
git commit -m "feat: add stacks" 
git push
```

---

## **TODO**

### **Security**

1. Add password for minio
2. Add api key for meilisearch
3. Protect horizon

### **Features**

1. Add telescope
2. Create sample project utilizing all the services
3. More image for caprover services
4. More deployment style for caprover
5. Move all conf file to snippet so that the stack can be used as submodule
